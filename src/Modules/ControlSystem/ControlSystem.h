#pragma once

class ControlSystem
{
public:
    virtual void control() = 0;
    const uint8_t id;
};
